import sys
sys.path.append("target")
from time_map_pb2 import TimeMap, TimeMapResponse
from util.options import parse
from util.proto import invoke
from util.time import today

args = parse()
url = args.api_url + '/v3/time_map'

ts = today(hour=9, minute=0)

# text representation of request
text = """
params {
  api_key {
    app_id: "%s"
    app_key: "%s"
  }
}
format {
  type: TT
  holes: false
}
smooth: true
targets {
  id: 1
  coords {
    lat: 51.5
    lng: 0.12
  }
  travel_time: 600
  date {
    type: Start
    timestamp: %s
  }
  mode {
    type: PublicTransport
  }
}
targets {
  id: 2
  coords {
    lat: 51.46
    lng: 0.18
  }
  travel_time: 600
  date {
    type: Start
    timestamp:%s
  }
  mode {
    type: PublicTransport
  }
}
shapes {
  target: 1
}
shapes {
  target: 2
}
intersections {
  id: 3
  targets: 1
  targets: 2
}
unions {
  id: 4
  targets: 1
  targets: 2
}
""" % (args.app_id, args.app_key, ts, ts)

response = invoke(url, text, TimeMap(), TimeMapResponse())
print response
