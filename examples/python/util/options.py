import sys
from optparse import OptionParser

def parse():
  parser = OptionParser()
  parser.add_option('--app-id', type='string', dest='app_id',
                    help='your application id for authentication')
  parser.add_option('--app-key', type='string', dest='app_key',
                    help='your application key for authentication')
  parser.add_option('--api-url', type='string', dest='api_url',
                    help='TravelTimeAPI url', default='http://api.traveltimeapp.com')
  (opt, args) = parser.parse_args()

  if not opt.app_id:
    parser.print_help()
    sys.exit('`--app-id` not given')

  if not opt.app_key:
    parser.print_help()
    sys.exit('`--app-key` not given')

  return opt