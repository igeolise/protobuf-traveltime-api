import datetime
import calendar
import pytz

utc = pytz.utc

def today(hour, minute):
  return __timestamp(datetime.datetime.now(tz=utc).replace(hour=hour, minute=minute, second=0))

def __timestamp(date):
  return calendar.timegm(date.utctimetuple())
