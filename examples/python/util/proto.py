from google.protobuf import text_format
import urllib2
import lz4

CONTENT_TYPE = {"Content-Type": "application/x-protobuf"}
CONTENT_ENCODING = {"Content-Encoding": "lz4"}

def invoke(url, text, request_proto, response_proto, compress=True):
  data = __serialize(text, request_proto)
  headers = CONTENT_TYPE
  if compress:
    data = lz4.dumps(data)
    headers.update(CONTENT_ENCODING)
  request = urllib2.Request(url, data, headers)
  try:
    response = urllib2.urlopen(request).read()
  except urllib2.HTTPError, error:
    response = error.read()
  if compress:
    response = lz4.loads(response)
  return __deserialize(response, response_proto)

def __serialize(text, message):
  text_format.Merge(text, message)
  return message.SerializeToString()

def __deserialize(bytes, message):
  message.ParseFromString(bytes)
  return message