import sys
sys.path.append('target')
from has_data_pb2 import HasData, HasDataResponse
from util.options import parse
from util.proto import invoke

args = parse()
url = args.api_url + '/has_data'

# text representation of request
text = """
params {
  api_key {
    app_id: "%s"
    app_key: "%s"
  }
}
coords {
  lat: 51.5
  lng: 0.12
}
""" % (args.app_id, args.app_key)

response = invoke(url, text, HasData(), HasDataResponse())
print response
