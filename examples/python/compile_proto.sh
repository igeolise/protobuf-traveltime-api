#!/bin/bash

DIR=../../src/main/protobuf

# compile all proto files
mkdir -p target
for proto in $DIR/*.proto
do
  protoc --proto_path $DIR --python_out=./target $proto
done
