import sys
sys.path.append("target")
from time_filter_pb2 import TimeFilter, TimeFilterResponse
from util.options import parse
from util.proto import invoke
from util.time import today

args = parse()
url = args.api_url + '/v3/time_filter'

# text representation of request
text = """
params {
  api_key {
    app_id: "%s"
    app_key: "%s"
  }
}
points {
  id: 1
  coords {
    lat: 51.5
    lng: 0.12
  }
}
sources {
  id: 1
  travel_time: 3600
  coords {
    lat: 51.46
    lng: 0.18
  }
  mode {
    type: PublicTransport
  }
  properties: Time
  timestamp: %s
}
""" % (args.app_id, args.app_key, today(hour=9, minute=0))

response = invoke(url, text, TimeFilter(), TimeFilterResponse())
print response
