package traveltimeapp;

option java_package = "com.traveltimeapp.protobuf";
option java_outer_classname = "TimeFilterProtos";

import "common.proto";

// Given origin and destination points find out properties
// of connections between origin and destination points.
message TimeFilter {
  message Point {
    required uint32 id = 1;
    required Coords coords = 2;
  }

  // Specifies which data properties you want to know about the points.
  enum RequestProperties {
    Time = 0;
    Distance = 1;
    DistanceBreakdown = 2;
  }

  // Destination or source parameters
  message Target {
    required uint32 id = 1;
    // Travel time in seconds.
    required uint32 travel_time = 2;
    required Coords coords = 3;
    required TransportationMode mode = 4;
    repeated RequestProperties properties = 5;

    // POSIX time in seconds
    required uint32 timestamp = 6;
  }

  required Params params = 1;
  repeated Point points = 2;

  // We start our journey from each source and arrive at each point.
  repeated Target sources = 3;

  // We start our journey from each point and arrive at each destination.
  repeated Target destinations = 4;

  // Relevant only for public transportation modes.
  // If false (default) travel time to destinations (from sources) includes
  // waiting time for the first vehicle at the origin point. If true waiting
  // time is removed.
  optional bool remove_wait_time = 5;
}

message TimeFilterResponse {
  message DistanceBreakdown {
    required EdgeType type = 1;
    // Distance in meters.
    required uint32 distance = 2;
  }

  // Point will hold information you specified in request properties.
  message Point {
    required uint32 id = 1;
    optional uint32 time = 2;
    optional uint32 distance = 3;
    repeated DistanceBreakdown distance_breakdowns = 4;
  }

  // Destination or source result.
  message Target {
    required uint32 id = 1;
    repeated Point points = 2;
    optional Error error = 3;
  }

  message Result {
    repeated Target sources = 1;
    repeated Target destinations = 2;
  }

  oneof response {
    Result result = 1;
    Error error = 2;
  }
}