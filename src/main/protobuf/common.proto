package traveltimeapp;

option java_package = "com.traveltimeapp.protobuf";
option java_outer_classname = "Common";

message Error {
  enum Code {
    // Internal server failures.
    Server = 0;

    // Given parameters are not valid.
    BadParams = 1;

    // Auth params are missing or invalid.
    BadAuthParams = 2;

    // Auth params are in correct format but are unknown to us.
    NoAuth = 3;

    // Request limit for a time frame is reached.
    LimitReached = 4;

    // We have the data set, but there are no useful data around a given location.
    OutOfReach = 5;

    // When we do not have any data in given location.
    NoData = 6;

    // Request processing did not finish in predefined time.
    TimedOut = 7;

    // We don't have data that covers this date.
    NoDataForGivenDate = 8;
  }

  required Code code = 1;

  // Details providing further information on error.
  optional string details = 2;
}

// Authentication params.
message ApiKey {
  required string app_id = 1;
  required string app_key = 2;
}

// Coordinates in the WGS 84 format, in degrees.
message Coords {
	required float lat = 1;
	required float lng = 2;
}

enum EdgeType {
  Bike = 0;
  Bus = 1;
  CableCar = 3;
  Car = 4;
  Coach = 5;
  Parking = 6;
  Plane = 7;
  RailDlr = 8;
  RailNational = 9;
  RailOverground = 10;
  RailUnderground = 11;
  Ship = 12;
  Train = 13;
  Walk = 14;
}

message TransportationMode {
  enum Type {
    Cycling = 0;
    Driving = 1;
    DrivingTrain = 2;
    PublicTransport = 3;
    Walking = 4;
    WalkingCoach = 5;
    WalkingBus = 6;
    WalkingTrain = 7;
  }

  // Extra params for all public transportation modes.
  // None of the parameters can be more than `travel_time`.
  message PtParams {
    enum Mode {
      SingleRoute = 0;
      MultipleRoutes = 1;
    }

    // Maximum time spent traveling to first (from last) station.
    optional uint32 time_to_station_limit = 1;

    // Time needed to board public transportation vehicle (in seconds)
    optional uint32 pt_change_penalty = 2;

    // Relevant only for v3\time_map or v3\time_filter requests.
    // when Mode.SingleRoute (default) - single fastest route is returned
    // when Mode.MultipleRoutes - a few routes are returned where each of
    // them requires different amount of transfers
    optional Mode routing_mode = 3;
  }

  // Extra params for `DrivingTrain` mode.
  message DrivingTrainParams {
    // Change delay between driving and walking (in seconds),
    // i.e. time needed to park a car and get a ticket.
    optional uint32 parking_time = 1;

    optional PtParams pt_params = 2;

    // Maximum time spent traveling from last station to target location
    // (is used instead of time_to_station_limit for TARGET LOCATION ONLY)
    optional uint32 walk_from_station_limit = 3;
  }

  required Type type = 1;

  oneof extra_params {
    PtParams pt_only_params = 2;
    DrivingTrainParams driving_train_params = 3;
  }
}

// Default `TravelTime` configuration override.
// Can be used in advanced cases to tweak algorithm behaviour,
// extend/reduce timeout, etc.
message ConfigOverride {
  message Value {
    oneof value {
      string str_value = 1;
      int32 int_value = 2;
      float float_value = 3;
      double double_value = 4;
      bool bool_value = 5;
    }
  }

  required string name = 1;
  required Value value = 2;
}

// Params used in all request messages.
message Params {
  required ApiKey api_key = 1;
  repeated ConfigOverride config_overrides = 2;
}
