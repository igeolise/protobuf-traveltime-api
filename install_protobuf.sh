#!/bin/bash

# Download, compile & install Protobuf compiler.

PROTO_VERSION=2.6.0
DOWNLOAD_BASE_URL=http://protobuf.googlecode.com/svn/rc
DOWNLOAD_DEST=/tmp

SOURCE_FOLDER='protobuf-'$PROTO_VERSION
SOURCE_TAR=$SOURCE_FOLDER'.tar.gz'

# download source
url=$DOWNLOAD_BASE_URL'/'$SOURCE_TAR
cd $DOWNLOAD_DEST && wget $url

# extract
tar -xvf $DOWNLOAD_DEST/$SOURCE_TAR

# compile and install
cd $DOWNLOAD_DEST/$SOURCE_FOLDER && \
  ./configure --prefix=/usr && \
  make && \
  sudo make install

# cleanup
rm -rf $DOWNLOAD_DEST/$SOURCE_FOLDER
rm $DOWNLOAD_DEST/$SOURCE_TAR