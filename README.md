

### Protobuf messages for TravelTime API.

#### Protobuf

Get it at: https://developers.google.com/protocol-buffers/

Use version __2.6.0__

#### Proto files 

Proto files are located at: src/main/protobuf.  
There's a separate .proto for every message type and additional .proto containing common types.  
Every request message has a corresponding response message. E.g. HasData & HasDataResponse.  
All request messages contain field Params.ApiKey that is used for authentication.  

#### Available messages

  * has_data

    Check if we have data for given coordinates.  
    /has_data 


  * time_filter

    Given origin and destination points find out properties of connections between origin and destination points.  
    /v3/time_filter


  * time_map

    Find shapes of zones reachable from origin given travel time and transportation mode.  
    /v3/time_map


#### HTTP request

API is available using HTTP endpoint.

  * serialize request Protobuf message to binary data.
  * make HTTP __POST__ request 
    * to http://api.traveltimeapp.com + message relative path (e.g. http://api.traveltimeapp.com/v3/time_map)
    * with binary data as payload
    * and HTTP header __Content-Type: application/x-protobuf__
  * read response as binary data.
  * deserialize corresponding response message from response binary data.


#### Compression

We support (and recommend to use) LZ4 compression.  
It is available for most of the popular programming languages.  
Get it at: https://code.google.com/p/lz4  

To use it:

  * compress request binary data
  * add HTTP header __Content-Encoding: lz4__
  * response data will also be compressed 


#### Runnable examples

##### Python

Scripts are located at __examples/python__.  
Examples are in Python 2.x.  
To run the examples you must have Python libraries installed:

  * protobuf
  * pytz
  * lz4
  
All mentioned dependencies can be installed using __pip__, i.e. *pip install lz4*.  

  * compile .proto files into example/proto/target directory  
    There's a bash script __compile_proto.sh__.

  * run example scripts providing __--app-id__ and __--app-key__ arguments.  
    E.g. *python2 has_data.py --app-id=1234 --app-key=abcd*