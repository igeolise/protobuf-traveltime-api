addSbtPlugin("com.github.gseitz" % "sbt-protobuf" % "0.3.3")
addSbtPlugin("com.igeolise.sbt" % "artifactory-plugin" % "1.1.0")

resolvers := Seq(
  "iGeolise releases" at "https://master.traveltimeapp.com:8081/artifactory/libs-release",
  Resolver.url("iGeolise ivy releases", url("https://master.traveltimeapp.com:8081/artifactory/libs-release-ivy"))(Resolver.ivyStylePatterns)
)
externalResolvers := Resolver.defaultLocal +: resolvers.value
fullResolvers := projectResolver.value +: externalResolvers.value
 
credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")