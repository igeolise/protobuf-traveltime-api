import sbtprotobuf.{ProtobufPlugin=>PB}

Seq(PB.protobufSettings: _*)

version in protobufConfig := "2.6.0"

organization := "com.igeolise.traveltime"

name := "api-protobuf"

version := "1.0.0"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7", "-g")

javacOptions in doc := (if(sys.props("java.specification.version") == "1.8")
  Seq("-Xdoclint:none")
else
  Seq()
)

// Do not append Scala versions to the generated artifacts
crossPaths := false

// This forbids including Scala related libraries into the dependency
autoScalaLibrary := false

igeoliseResolverSettings